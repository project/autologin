Autologin
=========

Autologin enables easy user login by navigating to the internal Drupal path
'login/<username>/<password>'. For more information, issue tracking and feature
suggestions, please see the project page.


Installation
------------

Install Autologin as follows:

  1. Download the latest stable version of Autlogin from its project page.
  3. Unpack the downloaded file into sites/all/modules or the modules directory
     of your site.


Author
------

The module was developed by Tobias Sjösten <http://drupal.org/user/213383>.
The author can be contacted for paid customizations of this module as well as
Drupal consulting, installation, development, and customizations.


Sponsors
--------

The development of this module has been sponsored by

* SSPA <http://www.sspa.se/>
* Webbredaktören <http://www.webbredaktoren.se/>
